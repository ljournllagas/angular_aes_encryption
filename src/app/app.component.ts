import { Component } from '@angular/core';  
import * as CryptoJS from 'crypto-js'; 

@Component({  
  selector: 'app-root',  
  templateUrl: './app.component.html',  
  styleUrls: ['./app.component.css']  
})  
export class AppComponent {  
  title = 'EncryptionDecryptionSample';  
    
  plainText:string;  
  encryptText: string;  
  encPassword: string;  
  conversionEncryptOutput: string;  
    
  constructor() {  
  }  
  //method is used to encrypt and decrypt the text  
  convertText() {  
        this.conversionEncryptOutput = this.encryptData(this.encPassword.trim(), this.plainText.trim()); //CryptoJS.AES.encrypt(this.plainText.trim(), this.encPassword.trim()).toString();  
  }
  
  //Data Encryption Function
  encryptData(key, toEncrypt) {
    var keySize = 256;
    var salt = CryptoJS.lib.WordArray.random(16);
    var encKey = CryptoJS.PBKDF2(key, salt, {
        keySize: keySize / 32,
        iterations: 100
    });
    
    var iv = CryptoJS.lib.WordArray.random(128 / 8);
    
    var encrypted = CryptoJS.AES.encrypt(toEncrypt, encKey, {
        iv: iv,
        padding: CryptoJS.pad.Pkcs7,
        mode: CryptoJS.mode.CBC
    });
    
    var result = CryptoJS.enc.Base64.stringify(salt.concat(iv).concat(encrypted.ciphertext));
    
    return result;
  }

}  